#include "selfrestart.c"
#include "killunsel.c"
#include "winview.c"
#include <X11/XF86keysym.h>

/* See LICENSE file for copyright and license details. */
/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappx     = 4;        /* pixel gap between clients */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 0;        /* horizontal padding for statusbar */
static const int vertpadbar         = 0;        /* vertical padding for statusbar */
static const char *fonts[]          = { "sans:size=10" };
static const char dmenufont[]       = "sans:size=10";
static const char col_gray1[]       = "#282828";
static const char col_gray2[]       = "#282828"; /* border color unfocused windows */
static const char col_gray3[]       = "#96b5b4";
static const char col_gray4[]       = "#c0c5ce";
static const char col_cyan[]        = "#458588"; /* border color focused windows and tags */
/* static const unsigned int baralpha = 0x5a; */
static const unsigned int baralpha = 0xff;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
/* static const char *tags[] = { "Ranger", "Vim", "Emacs", "Surf", "Web", "O&G", "Mail", "System", "Video", "VBox" }; */


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	/* {NULL,      "wps",       NULL,       1 << 5,       0,           -1 }, */
	/* {NULL,      "libreoffice",       NULL,       1 << 5,       0,           -1 }, */
	/* {"writer",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"calc",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"impress",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"draw",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"Gimp",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"RawTherapee",      NULL,       NULL,       1 << 5,       0,           -1 }, */
	/* {"Firefox",   NULL,       NULL,       1 << 4,       0,           -1 }, */
	/* {"Chromium",   NULL,       NULL,       1 << 4,       0,           -1 }, */
	/* {"tabbed",	NULL,	NULL,       1 << 3,       0,           -1 }, */
	/* {"VirtualBox",   NULL,       NULL,       1 << 9,       0,           -1 }, */
	/* {"Surf",      NULL,       NULL,       1 << 3,       0,           -1 }, */
	/* {"mpv",       NULL,       NULL,       1 << 8,       0,           -1 }, */
	/* {NULL,        "st",       "neomutt",  1 << 6,       0,           -1 }, */
	{NULL,        "st",       "fst",      0,            1,           -1 },
	/* {NULL,        "st",       "franger",  0,            1,           -1 }, */
	/* {NULL,        "st",       "ranger",   1 << 0,       0,           -1 }, */
	/* {NULL,        "urxvt",       "ranger",   1 << 0,       0,           -1 }, */
	/* {NULL,        "st",       "r",        1 << 0,       0,           -1 }, */
	/* {NULL,        "st",       "htop",     1 << 7,       0,           -1 }, */
	/* {NULL,        "st",       "em",       1 << 2,       0,           -1 }, */
	/* {"Emacs",     NULL,       NULL,       1 << 2,       0,           -1 }, */
	/* {"nvim-qt",     NULL,       NULL,       1 << 1,       0,           -1 }, */
	/* {NULL,        "st",       "v",        1 << 1,       0,           -1 }, */
	/* {NULL,        "st",       "nvim",     1 << 1,       0,           -1 }, */
	/* {NULL,	      NULL,       "dropdowncalc",       0,            1,           -1 }, */
	/* {NULL,	      "telegram-desktop",     "Telegram",   1 << 6,      0,           -1 }, */
};

/* layout(s) */
static const float mfact     = 0.70; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
#include "centeredmaster.c"
#include "bottomstack.c"
#include "horizgrid.c"
#include "nextprevtag.c"
#include "maximize.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "###",      horizgrid },
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "H[]",      deck },
	/* { "|M|",      centeredmaster }, */
	{ ">M>",      centeredfloatingmaster },
	{ "TTT",      bstack },
	/* { "===",      bstackhoriz }, */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }


/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
/*static const char *termcmd[]  = { "st", NULL };*/
static const char *termcmd[]  = { "st", NULL };
static const char *ftermcmd[]  = { "st", "-t fst", NULL };
static const char scratchpadname[] = "scratchpad";
/* static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", NULL }; */
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-e", "tmuxdd", "-g", "120x34", NULL };
static const char *brightup[]  = { "xbacklight", "-inc", "10", NULL };
static const char *brightdown[]  = { "xbacklight", "-dec", "10", NULL };

static Key keys[] = {
	/* modifier               key              function        argument */
	{ MODKEY,       	  XK_semicolon,    	   spawn,          {.v = dmenucmd } },
	{ MODKEY,                 XK_Return,       spawn,          {.v = termcmd } },
	{ MODKEY,                 XK_s,  	   togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                 XK_space,        winview,        {0} },
	{ MODKEY,                 XK_b,            togglebar,      {0} },
	{ MODKEY|ShiftMask,       XK_j,            rotatestack,    {.i = -1 } },
	{ MODKEY|ShiftMask,       XK_k,            rotatestack,    {.i = +1 } },
	{ MODKEY,                 XK_j,            focusstack,     {.i = -1 } },
	{ MODKEY,                 XK_k,            focusstack,     {.i = +1 } },
	{ MODKEY,       	  XK_i,            incnmaster,     {.i = +1 } },
	{ MODKEY,       	  XK_d,            incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,                 XK_h,            setmfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,                 XK_l,            setmfact,       {.f = +0.05} },
	{ MODKEY,              XK_l,           view_adjacent,  { .i = +1 } },
	{ MODKEY,              XK_h,           view_adjacent,  { .i = -1 } },
	{ MODKEY|ShiftMask,       XK_Return,       zoom,           {0} },
	{ MODKEY,                 XK_Tab,          view,           {0} },
	{ MODKEY,       	  XK_q,            killclient,     {0} },
	{ MODKEY|ShiftMask,       XK_q,            killunsel,      {0} },
	{ MODKEY,                 XK_v,            setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                 XK_t,            setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                 XK_f,            setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                 XK_m,            setlayout,      {.v = &layouts[3]} },
	{ MODKEY,             	  XK_g,            setlayout,      {.v = &layouts[4]} },
	/* { MODKEY|ShiftMask,       XK_o,      	   setlayout,      {.v = &layouts[4]} }, */
	{ MODKEY,                 XK_o,      	   setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                 XK_u,            setlayout,      {.v = &layouts[6]} },
	/* { MODKEY|ShiftMask,       XK_u,            setlayout,      {.v = &layouts[7]} }, */
	/* { MODKEY,                 XK_space,        setlayout,      {0} }, */
	{ MODKEY|ShiftMask,       XK_space,        togglefloating, {0} },
	{ MODKEY,                 XK_p,            view,           {.ui = ~0 } },
	/* { MODKEY,                 XK_0,            view,           {.ui = ~0 } }, */
	{ MODKEY|ShiftMask,       XK_0,            tag,            {.ui = ~0 } },
	{ MODKEY,                 XK_comma,        focusmon,       {.i = -1 } },
	{ MODKEY,                 XK_period,       focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,       XK_comma,        tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,       XK_period,       tagmon,         {.i = +1 } },
	/* { MODKEY|ControlMask,     XK_comma,        cyclelayout,    {.i = -1 } }, */
	/* { MODKEY|ControlMask,     XK_period,       cyclelayout,    {.i = +1 } }, */

	/* Maximize */
	{ MODKEY|ControlMask, 		XK_h,           togglehorizontalmax, NULL },
	/* { MODKEY|ControlMask|ShiftMask, XK_l,           togglehorizontalmax, NULL }, */
	{ MODKEY|ControlMask, 		XK_j,           toggleverticalmax,   NULL },
	/* { MODKEY|ControlMask|ShiftMask, XK_k,           toggleverticalmax,   NULL }, */
	{ MODKEY|ControlMask,           XK_f,           togglemaximize,      {0} },

    	/* Volume Controls  */
	{ MODKEY,       	  XK_equal,       spawn,          CMD("lmc up 5") },
	{ MODKEY|ShiftMask,       XK_equal,       spawn,          CMD("lmc up 15") },
	{ MODKEY,       	  XK_minus,       spawn,          CMD("lmc down 5") },
	{ MODKEY|ShiftMask,       XK_minus,       spawn,          CMD("lmc down 15") },

	/* Launchers */
	{ MODKEY|ShiftMask,       XK_a,       spawn,          CMD("st -e pulsemixer") },
	{ MODKEY,       	  XK_r,       spawn,          CMD("st -c ranger -e ranger") },
	{ MODKEY|ShiftMask,       XK_r,       spawn,          CMD("urxvt -e ranger -r $HOME/.config/ranger_urxvt") },
	{ MODKEY,       	  XK_z,       spawn,          CMD("st -e nvim -u $HOME/.phpvimrc") },
	{ MODKEY|ShiftMask,       XK_z,       spawn,          CMD("emacsclient -c") },
	{ MODKEY,       	  XK_a,       spawn,          CMD("dmenuopener") },
	{ MODKEY,       	  XK_e,       spawn,          CMD("st -e neomutt") },
	{ MODKEY|ControlMask,     XK_b,       spawn,          CMD("check-status") },
	{ MODKEY,                 XK_w,       spawn,          CMD("tabbed -n VimB -f vimb -e") },
	{ MODKEY|ShiftMask,       XK_i,       spawn,          CMD("st -e htop") },
	{ MODKEY|ShiftMask,       XK_w,       spawn,          CMD("firefox") },
	{ MODKEY|ControlMask,     XK_w,       spawn,          CMD("chromium") },
	{ MODKEY,       	  XK_c,       spawn,          CMD("clipmenu") },
	{ MODKEY|ShiftMask,       XK_c,       spawn,          CMD("camtoggle") },
	{ MODKEY,       	  XK_n,       spawn,          CMD("oprutess") },
	{ MODKEY|ShiftMask,       XK_y,       spawn,          CMD("st -e youtube-viewer") },
	{ MODKEY,       	  XK_y,       spawn,          CMD("st -e calcurse") },
	{ MODKEY|ShiftMask,       XK_n,       spawn,          CMD("st -e newsboat") },
	{ MODKEY|ControlMask,     XK_e,       spawn,          CMD("telegram-desktop") },
	{ MODKEY|ShiftMask,       XK_e,       spawn,          CMD("surf vk.com/im") },
	{ MODKEY,       	  XK_grave,   spawn,          CMD("dmenuunicode") },

	/* X key */
	{ MODKEY,       	XK_x,        spawn,          CMD("goldendict $(xclip -o -selection primary)") },
	{ MODKEY|ShiftMask,     XK_x,        spawn,          CMD("prompt 'Shutdown?' 'doas shutdown -h now'") },
	{ MODKEY|ControlMask,   XK_x,        spawn,          CMD("prompt 'Reboot?' 'doas reboot'") },

	/* Tags */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)

	/* Top row */
	{ MODKEY|ControlMask,   XK_Escape,   quit,		{0} },
	{ MODKEY, 		XK_F2, 	     self_restart,	{0} },
	{ MODKEY,       	XK_F3,       spawn,          CMD("displayselect") },
	{ MODKEY,       	XK_F4,       spawn,          CMD("prompt 'Suspend?' 'systemctl suspend'") },
	{ MODKEY,       	XK_F5,       spawn,          CMD("doas systemctl restart NetworkManager") },
	{ MODKEY,       	XK_F6,       spawn,          CMD("st -e transmission-remote-cli") },
	{ MODKEY,       	XK_F7,       spawn,          CMD("td-toggle") },
	{ MODKEY,       	XK_F8,       spawn,          CMD("~/.config/mutt/etc/mailsync.sh") },
	{ MODKEY,       	XK_F9,       spawn,          CMD("dmenumount") },
	{ MODKEY,       	XK_F10,      spawn,          CMD("dmenuumount") },
	{ MODKEY,       	XK_F11,      spawn,          CMD("ducksearch") },
	{ MODKEY,       	XK_F12,      spawn,          CMD("st -e nmtui") },
	{ MODKEY,       	XK_Insert,   spawn,          CMD("showclip") },
	{ MODKEY,       	XK_Pause,    spawn,          CMD("xcqr") },
	{ 0,       		XK_Print,    spawn,          CMD("scrot") },
	{ ShiftMask,       	XK_Print,    spawn,          CMD("scrot -u") },
	{ MODKEY,       	XK_Print,    spawn,          CMD("dmenurecord") },
	{ MODKEY,       	XK_Delete,   spawn,          CMD("stoprec") },

	/* XF86 Keys */
	{ 0,       	XF86XK_AudioRaiseVolume,      spawn,          CMD("lmc up 5") },
	{ 0,       	XF86XK_AudioLowerVolume,      spawn,          CMD("lmc down 5") },
	{ 0,       	XF86XK_AudioMute,      	      spawn,          CMD("lmc mute") },
	{ 0,       	XF86XK_AudioMicMute,          spawn,          CMD("sbuckle") },
	/* { 0,       	XF86XK_MonBrightnessUp,       spawn,          CMD("xbacklight -inc 10") }, */
	/* { 0,       	XF86XK_MonBrightnessDown,     spawn,          CMD("xbacklight -dec 10") }, */
	{ 0,       	XF86XK_MonBrightnessUp,       spawn,          {.v = brightup} },
	{ 0,       	XF86XK_MonBrightnessDown,     spawn,          {.v = brightdown} },
	{ 0,       	XF86XK_PowerOff,      	      spawn,          CMD("prompt 'Shutdown?' 'doas shutdown -h now'") },
	{ 0,       	XF86XK_Display,      	      spawn,          CMD("arandr") },
	{ 0,       	XF86XK_Hibernate,      	      spawn,          CMD("prompt 'Hibernate?' 'systemctl hibernate'") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = ftermcmd } },
	{ ClkStatusText,        MODKEY,         Button2,        spawn,          {.v = dmenucmd} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
